module github.com/nosebit/act

go 1.16

require (
	github.com/hpcloud/tail v1.0.0 // indirect
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
	gopkg.in/fsnotify.v1 v1.4.7 // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
